package org.example;

public class LFUCacheStatistics implements Subscriber {
  /**updates statistics information, implementation.
   * @param eventType type of event (put, get etc.)
   * @param statistics contains cache statistics*/
  @Override
  public void update(final String eventType, final Statistics statistics) {
    System.out.println("Record was: " + eventType);
    System.out.println("Cache size: " + statistics.getCacheSize());
    System.out.println("Highest frequency: " + statistics.getHighestFreq());
    System.out.println("Lowest frequency: " + statistics.getLowestFreq());
    System.out.println("Deleted log size: "
            + statistics.getDeletedSize() + "\n");
  }
}
