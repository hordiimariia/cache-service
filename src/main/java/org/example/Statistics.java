package org.example;

public class Statistics {
  /**Current cache size.*/
  private int cacheSize;
  /**Highest frequency in cache.*/
  private int highestFreq;
  /**Lowest frequency in cache.*/
  private int lowestFreq;
  /**Amount of deleted items.*/
  private int deletedSize;

  /**Statistics constructor.*/
  public Statistics() {

  }

  /** Gets the cache size.
   * @return cacheSize*/
  public int getCacheSize() {
    return cacheSize;
  }

  /** Sets the cache current size.
   * @param cacheSizeToSet cache size*/
  public void setCacheSize(final int cacheSizeToSet) {
    this.cacheSize = cacheSizeToSet;
  }

  /** Gets the highest frequency.
   * @return highestFreq*/
  public int getHighestFreq() {
    return highestFreq;
  }

  /** Sets the setHighestFreq variable with the highest frequency.
   * @param highestFreqToSet the lowest frequency*/
  public void setHighestFreq(final int highestFreqToSet) {
    this.highestFreq = highestFreqToSet;
  }

  /** Gets the lowest frequency.
   * @return lowestFreq*/
  public int getLowestFreq() {
    return lowestFreq;
  }

  /** Sets the setLowestFreq variable with the lowest frequency.
   * @param lowestFreqToSet the lowest frequency*/
  public void setLowestFreq(final int lowestFreqToSet) {
    lowestFreq = lowestFreqToSet;
  }

  /** Gets the amount of deleted records.
   * @return deletedSize */
  public int getDeletedSize() {
    return deletedSize;
  }

  /** Sets the deletedSize variable with amount of deleted records.
   * @param deletedSizeToSet amount of deleted records*/
  public void setDeletedSize(final int deletedSizeToSet) {
    this.deletedSize = deletedSizeToSet;
  }
}
