package org.example;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import java.util.concurrent.TimeUnit;

public class GuavaLruCache {
  /**cache variable.*/
  Cache<Long, Name> cache;

  /**Gets cache size.
   *@return cache size*/
  public long getSize() {
    return cache.size();
  }
  /**removalListener.*/
  RemovalListener<Long, Name> removalListener = notification ->
      System.out.println("Key: " + notification.getKey()
              + ", Value: " + notification.getValue()
              + ", Cause: " + notification.getCause());

  /**Max time for cache records.*/
  final long durationSeconds = 5;
  /**Max cache size.*/
  final long maxCacheSize = 100000;

  /**GuavaLruCache constructor.*/
  public GuavaLruCache() {
    cache = CacheBuilder.newBuilder()
        .maximumSize(maxCacheSize)
            .expireAfterAccess(durationSeconds, TimeUnit.SECONDS)
            .removalListener(removalListener).build();
  }

  /**GuavaLruCache constructor.
   * @param maxSize is to define cache max size*/
  public GuavaLruCache(final int maxSize) {
    cache = CacheBuilder.newBuilder()
        .maximumSize(maxSize)
            .expireAfterAccess(durationSeconds, TimeUnit.SECONDS)
            .removalListener(removalListener).build();
  }

  /**Puts Name to the cache.
   * @param key key
   * @param value Name*/
  public void put(final Long key, final Name value) {
    cache.put(key, value);
  }

  /**Gets Name from the cache.
   * @param key is key where stored Name
   * @return name*/
  public Name get(final Long key) {
    return cache.getIfPresent(key);
  }
}
