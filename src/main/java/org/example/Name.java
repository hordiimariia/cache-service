package org.example;

import java.util.Objects;

public class Name {
    /**name.*/
    private String name;

    /**Name constructor.
     * @param nameInit initializes the name field*/
    public Name(final String nameInit) {
        this.name = nameInit;
    }

    /**Gets Name.
     *@return name*/
    public String getName() {
        return name;
    }

    /**Sets the Name.
     * @param nameToSet is Name*/
    public void setName(final String nameToSet) {
        this.name = nameToSet;
    }

    /**overridden equals.*/
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Name name1 = (Name) o;
        return Objects.equals(name, name1.name);
    }

    /**overridden hashCode.*/
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
