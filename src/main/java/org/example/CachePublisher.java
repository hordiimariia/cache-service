package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CachePublisher {
  /**operationListeners.*/
  Map<String, List<Subscriber>> operationListeners = new HashMap<>();

  /**CachePublisher constructor.
   * @param operations list if the event types (get, put etc.)*/
  public CachePublisher(final String... operations) {
    for (String operation : operations) {
      this.operationListeners.put(operation, new ArrayList<>());
    }
  }

  /**Subsribes for changes.
   * @param eventType type of event (get, put etc.)
   * @param subscriber subscriber on events*/
  public void subscribe(final String eventType, final Subscriber subscriber) {
    List<Subscriber> subscribers = operationListeners.get(eventType);
    subscribers.add(subscriber);
  }

  /**Unsubscribes from changes.
   * @param eventType type of event (get, put etc.)
   * @param subscriber subscriber on events*/
  public void unsubscribe(final String eventType, final Subscriber subscriber) {
    List<Subscriber> subscribers = operationListeners.get(eventType);
    subscribers.remove(subscriber);
  }

  /**Notifies subscribers about changes.
   * @param eventType type of event (get, put etc.)
   * @param statistics cache statistics*/
  public void notify(final String eventType, final Statistics statistics) {
    List<Subscriber> users = operationListeners.get(eventType);
    for (Subscriber listener : users) {
      listener.update(eventType, statistics);
    }
  }
}
