package org.example;

public final class Main {
    private Main() { }
    /**main method.
     * @param args main arguments*/
    public static void main(final String[] args) {
        final long id1 = 1;
        final long id2 = 2;
        final long id3 = 3;
        final long id4 = 4;

        Name james = new Name("James");
        Name mary = new Name("Mary");
        Name robert = new Name("Robert");
        Name patricia = new Name("Patricia");

        LFUCache cache = new LFUCache();
        cache.events.subscribe("added", new LFUCacheStatistics());
        cache.events.subscribe("removed", new LFUCacheStatistics());
        cache.events.subscribe("reviewed", new LFUCacheStatistics());

        System.out.println("\nInsert " + james.getName());
        cache.put(id1, james);

        System.out.println("\nInsert " + mary.getName());
        cache.put(id2, mary);

        System.out.println("\nInsert " + james.getName());
        cache.put(id1, james);

        System.out.println("\nInsert " + robert.getName());
        cache.put(id3, robert);

        System.out.println("\nInsert " + james.getName());
        cache.put(id1, james);

        System.out.println("\nInsert " + patricia.getName());
        cache.put(id4, patricia);

        System.out.println("\nInsert " + mary.getName());
        cache.put(id2, mary);


        System.out.println("\nGet " + cache.get(id2).getName());
        cache.get(id2);
        System.out.println("\nGet " + cache.get(id3).getName());
        cache.get(id3);
        System.out.println("\nGet " + cache.get(id1).getName());
        cache.get(id1);
    }
}
