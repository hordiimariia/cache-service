package org.example;

public interface Subscriber {
    /** Updates statistics about cache.
     * @param eventType says what have been done (put/get/update...)
     * @param statistics contains info about cache size and other*/
    void update(String eventType, Statistics statistics);
  }
