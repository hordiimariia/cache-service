package org.example;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**.
 * <h1>LFU Cache</h1>
 * The LFU Cache program implements a cache
 *
 * @author  Mariia Hordii
 * @version 1.0
 * @since   2023-05-16
 */

public class LFUCache {
    /**CachePublisher variable.*/
    public CachePublisher events;
    /**Statistics variable.*/
    Statistics statistics;
    /** Cache max size variable. */
    private final int cacheMaxSize;
    /** Cache size constant variable. */
//    private static final int CACHE_SIZE = 100000;
    private static final int CACHE_SIZE = 3;
    /**Expiration time constant variable.*/
    private static final int EXPIRATION_TIME_IN_SECONDS = 5;
    /** Cache current size variable. */
    private int cacheCurrentSize;
    /** Minimal record frequency in cache. */
    private int minFrequency;
    /** HashMap for keys and values. */
    private final HashMap<Long, Name> keyValue;
    /** HashMap for keys and frequencies. */
    private final HashMap<Long, Integer> keyFrequency;

    /**Gets key to frequency HashMap.
     * @return keyFrequency*/
    public HashMap<Long, Integer> getKeyFrequency() {
        return keyFrequency;
    }

    /** TreeMap for frequencies and keys with time. */
    private final TreeMap<Integer,
        LinkedHashMap<Long, Long>> frequencyToKeysAndTime;
    /** HashMap to store delete records. */
    private final HashMap<Long, Name> deleted = new HashMap<>();

    /** Gets deleted records.
     * @return deleted records. */
    public HashMap<Long, Name> getDeleted() {
        return deleted;
    }

    /** @return cache current size */
    public int getCacheCurrentSize() {
        return cacheCurrentSize;
    }

    /** Default constructor. */
    public LFUCache() {
        this.cacheMaxSize = CACHE_SIZE;
        this.cacheCurrentSize = 0;
        this.minFrequency = 0;
        this.keyValue = new HashMap<>(cacheMaxSize);
        this.keyFrequency = new HashMap<>(cacheMaxSize);
        this.frequencyToKeysAndTime = new TreeMap<>();
        this.statistics = new Statistics();
        this.events = new CachePublisher("added",
                "removed", "reviewed", "updated");
    }

    /** Constructor for choosing cache size.
     * @param size defines cache size */
    public LFUCache(final int size) {
        this.cacheMaxSize = size;
        this.cacheCurrentSize = 0;
        this.minFrequency = 0;
        this.keyValue = new HashMap<>(cacheMaxSize);
        this.keyFrequency = new HashMap<>(cacheMaxSize);
        this.frequencyToKeysAndTime = new TreeMap<>();
        this.statistics = new Statistics();
        this.events = new CachePublisher("added",
                "removed", "reviewed", "updated");
    }

    /** Put object to cache.
     * @param key record key
     * @param value Name */
    public void put(final Long key, final Name value) {
        // if key or value are null, exception occurs
        if (key == null || value == null) {
            throw new IllegalArgumentException();
        } else {
            //if object exist in cache
            if (keyValue.containsKey(key)) {
                //update Name frequency in all collections
                updateFreq(key);

                updateStatistics();
                events.notify("updated", statistics);
            } else { //if cache doesn't contain object
                //if cache is full
                if (cacheCurrentSize == cacheMaxSize) {
                    LinkedHashMap<Long, Long> minFreqRecord =
                            frequencyToKeysAndTime.get(minFrequency);
                    Long minFreqKey = minFreqRecord.keySet().iterator().next();

                    //remove records elder than 5 seconds
                    removeExpiredRecords(minFreqRecord);

                    //remove Name with minimal frequency from keyValue HashTable
                    keyValue.remove(minFreqKey);

                    //remove record from keyFrequency HashTable
                    keyFrequency.remove(minFreqKey);

                    //remove first key in keysTime LinkedHashMap
                    //with minimal frequency record in
                    //frequencyToKeysAndTime TreeMap
                    if (frequencyToKeysAndTime.get(minFrequency)
                            .keySet().iterator().hasNext()) {
                        Long firstKey = frequencyToKeysAndTime
                                .get(minFrequency).keySet().iterator().next();
                        frequencyToKeysAndTime.get(minFrequency)
                                .remove(firstKey);
                    } else {
                        frequencyToKeysAndTime.remove(minFrequency);
                    }

                    //store deleted value in
                    deleted.put(key, value);

                    cacheCurrentSize -= 1;

                    updateStatistics();
                    events.notify("removed", statistics);
                }

                //if new element doesn't exist in cache,
                //it has minimal frequency - 1
                minFrequency = 1;

                //put new record to keyValue HashTable
                keyValue.put(key, value);

                //put new record to keyFrequency HashTable
                keyFrequency.put(key, minFrequency);

                //put new record to frequencyKeys TreeMap
                LinkedHashMap<Long, Long> record = frequencyToKeysAndTime
                        .get(keyFrequency.get(key));
                if (record != null) {
                    frequencyToKeysAndTime.get(keyFrequency.get(key))
                            .put(key, System.currentTimeMillis());
                } else {
                    frequencyToKeysAndTime
                            .put(minFrequency, new LinkedHashMap<>());
                    frequencyToKeysAndTime.get(minFrequency)
                            .put(key, System.currentTimeMillis());
                }

                //update cache size
                cacheCurrentSize += 1;

                updateStatistics();
                events.notify("added", statistics);
            }
        }
    }


    /** Get object from cache.
     * @param key record key
     * @return Name */
    public Name get(final Long key) {
        if (!keyValue.containsKey(key)) {
            return new Name(key + " element does not exist!");
        } else {
            updateFreq(key);

            updateStatistics();
            events.notify("reviewed", statistics);
            return keyValue.get(key);
        }
    }

    private boolean isExpired(final Long time) {
        final long convertToMillis = 1000;
        long elapsedTime = System.currentTimeMillis() - time;
        long expirationTimeInMillis =
                EXPIRATION_TIME_IN_SECONDS * convertToMillis;
        return elapsedTime > expirationTimeInMillis;
    }

    private void removeExpiredRecords(final LinkedHashMap<Long, Long> keyTime) {
        for (Map.Entry<Long, Long> entry : keyTime.entrySet()) {
            if (isExpired(entry.getValue())) {
                keyTime.remove(entry.getKey());
                keyValue.remove(entry.getKey());
                keyFrequency.remove(entry.getKey());
            }
        }
    }

    private void updateFreq(final Long key) {
        // get frequency for given key
        int currentKeyFreq = keyFrequency.get(key);
        int newFreq = currentKeyFreq + 1;

        //remove key from frequencyToKeysAndTime TreeMap to change its frequency
        frequencyToKeysAndTime.get(currentKeyFreq).remove(key);

        //if frequencyToKeysAndTime TreeMap
        //for given frequency is empty - remove that record
        if (frequencyToKeysAndTime.get(currentKeyFreq).isEmpty()) {
            frequencyToKeysAndTime.remove(currentKeyFreq);
        }

        //update frequency in keyFrequency HashMap
        keyFrequency.replace(key, currentKeyFreq, newFreq);

        //update frequency in frequencyToKeysAndTime TreeMap
        LinkedHashMap<Long, Long> record = frequencyToKeysAndTime.get(newFreq);
        if (record != null) {
            frequencyToKeysAndTime.get(newFreq)
                    .put(key, System.currentTimeMillis());
        } else {
            frequencyToKeysAndTime.put(newFreq, new LinkedHashMap<>());
            frequencyToKeysAndTime.get(newFreq)
                    .put(key, System.currentTimeMillis());
        }

        //update minimal frequency
        minFrequency = frequencyToKeysAndTime.firstKey();
    }

    private void updateStatistics() {
        statistics.setCacheSize(cacheCurrentSize);
        statistics.setHighestFreq(frequencyToKeysAndTime.lastKey());
        statistics.setLowestFreq(frequencyToKeysAndTime.firstKey());
        statistics.setDeletedSize(deleted.size());
    }
}
