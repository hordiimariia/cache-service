import java.util.concurrent.TimeUnit;
import org.example.LFUCache;
import org.example.Name;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

//import static org.junit.jupiter.api.Assertions.

public class LFUCacheTest {
    LFUCache cache = new LFUCache();
    LFUCache cache1 = new LFUCache(2);

    @Test
    public void testNullCase(){
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> cache.put(null, null));
        assertNull(exception.getMessage());
    }

    @Test
    public void testGetPut(){
        int size = cache.getCacheCurrentSize();
        Name name = new Name("TestName");

        cache.put(1L, new Name("TestName"));
        assertEquals(cache.getCacheCurrentSize(), size+1);

        assertEquals(cache.get(1L).getName(), name.getName());
    }

    @Test
    public void testEviction(){
        cache1.put(1L, new Name("TestName1"));
        cache1.put(2L, new Name("TestName2"));
        cache1.put(3L, new Name("TestName3"));

        assertEquals(1, cache1.getDeleted().size());
    }

    @Test
    public void expirationTimeTest(){
        cache1.put(8540L, new Name("Anne"));
        cache1.put(9468L, new Name("John"));
        try {
            TimeUnit.SECONDS.sleep(6);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
        cache1.put(8540L, new Name("Anne"));
        cache1.put(8497L, new Name("Peter"));

        assertEquals(2, cache1.getCacheCurrentSize());
        assertEquals("Anne", cache1.get(8540L).getName());
        assertEquals("Peter", cache1.get(8497L).getName());
    }

    @Test
    public void cacheSizeLimitTest(){
        cache1.put(8540L, new Name("Anne"));
        cache1.put(9468L, new Name("John"));
        cache1.put(8497L, new Name("Peter"));

        cache1.put(8540L, new Name("Anne"));
        cache1.put(8540L, new Name("Anne"));
        cache1.put(8540L, new Name("Anne"));

        cache1.put(9468L, new Name("John"));

        assertEquals(2, cache1.getCacheCurrentSize());
    }

    @Test
    public void frequencyUpdateTest(){
        cache.put(8540L, new Name("Anne"));
        cache.put(9468L, new Name("John"));
        cache.put(8497L, new Name("Peter"));

        cache.put(8540L, new Name("Anne"));
        cache.put(8540L, new Name("Anne"));
        cache.put(8540L, new Name("Anne"));

        cache.put(9468L, new Name("John"));

        assertEquals(4, cache.getKeyFrequency().get(8540L));
        assertEquals(2, cache.getKeyFrequency().get(9468L));
        assertEquals(1, cache.getKeyFrequency().get(8497L));
    }

    @Test
    public void cacheMissTest(){
        Name name = cache.get(2684L);
        assertEquals("2684 element does not exist!", name.getName());
    }
}
