import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.concurrent.TimeUnit;
import org.example.GuavaLruCache;
import org.example.Name;
import org.junit.jupiter.api.Test;

public class LRUCacheTest {
  GuavaLruCache cache = new GuavaLruCache();
  GuavaLruCache cache1 = new GuavaLruCache(2);

  @Test
  public void testNullCase(){
    Throwable exception = assertThrows(NullPointerException.class, () -> cache.put(null, null));
    assertNull(exception.getMessage());
  }

  @Test
  public void testGetPut(){
    Name name = new Name("TestName");

    cache.put(1L, new Name("TestName"));
    assertEquals(cache.get(1L).getName(), name.getName());
  }

  @Test
  public void expirationTimeTest(){
    cache1.put(8540L, new Name("Anne"));
    cache1.put(9468L, new Name("John"));
    try {
      TimeUnit.SECONDS.sleep(6);
    }
    catch (InterruptedException e){
      e.printStackTrace();
    }
    cache1.put(8540L, new Name("Anne"));
    cache1.put(8497L, new Name("Peter"));

    assertEquals("Anne", cache1.get(8540L).getName());
    assertEquals("Peter", cache1.get(8497L).getName());
  }

  @Test
  public void cacheSizeLimitTest(){
    cache1.put(8540L, new Name("Anne"));
    cache1.put(9468L, new Name("John"));
    cache1.put(8497L, new Name("Peter"));

    cache1.put(8540L, new Name("Anne"));
    cache1.put(8540L, new Name("Anne"));
    cache1.put(8540L, new Name("Anne"));

    cache1.put(9468L, new Name("John"));

    assertEquals(2, cache1.getSize());
  }

  @Test
  public void cacheMissTest() {
    final long id = 2684;
    assertNull(cache.get(id));
  }
}
