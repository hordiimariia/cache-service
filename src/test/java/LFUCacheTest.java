import org.example.LFUCache;
import org.example.Name;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LFUCacheTest {
    LFUCache cache = new LFUCache();
    LFUCache cache1 = new LFUCache(2);

    @Test
    public void testNullCase(){
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> cache.put(null, null));
        assertNull(exception.getMessage());
    }

    @Test
    public void testGetPut(){
        int size = cache.getCurrentSize();
        Name name = new Name("TestName");

        cache.put(1L, new Name("TestName"));
        assertEquals(cache.getCurrentSize(), size+1);

        assertEquals(cache.get(1L).getName(), name.getName());
    }

    @Test
    public void testEviction(){
        cache1.put(1L, new Name("TestName1"));
        cache1.put(2L, new Name("TestName2"));
        cache1.put(3L, new Name("TestName3"));

        assertEquals(1, cache1.getDeleted().size());
    }
}
